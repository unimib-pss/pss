# Processo e Sviluppo del Software - 1st Assignment

## Gruppo

BSS Group

&bull; <u>817 052</u> &nbsp;&emsp; Balde Muhammad Yassin <br>
&bull; <u>856 450</u> &emsp; Bianchi Edoardo			 <br>
&bull; <u>903 586</u> &emsp; Sala Niccolò			     <br>

## Repository

https://gitlab.com/unimib-community/2022_assignment1_bbsgroup/-/tree/main

## Applicazione

Il codice realizzato e caricato per lo svolgimento dell’assignment descrive una semplice applicazione in Python che si interfaccia ad un’API - url: https://www.fishwatch.gov/api/species - incaricata di fornire informazioni su diverse specie di pesci.

## Pipeline
### 1. Image
### 2. Variables
Con “variables” definiamo “PIP_CACHE_DIR” che indica il percorso della cartella della cache del progetto.
### 3. Cache
### 4. Stages
Attraverso “stages” definiamo quali sono le fasi della nostra pipeline.   Possiamo quindi identificare Build, Verify, Unit-test, Integration-test, Package, Release e Deploy.
### 4.1 Build

### 4.2 Verify

### 4.3 Unit-test

### 4.4 Integration-test

### 4.5 Package

### 4.6 Release

### 4.7 Deploy
